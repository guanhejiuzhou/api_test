#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: log
@time: 2021/12/1 22:29
"""

import logging


class MyLog(logging.Logger):

    def __init__(self, name='Log', level='DEBUG', file=None,
                 fmt='%(filename)s:%(lineno)d-%(asctime)s:%(name)s:%(levelname)s-%(message)s'):
        # 实例化对象
        super().__init__(name)

        # 设置级别
        self.setLevel(level)
        # 格式
        fmt = logging.Formatter(fmt)

        # 初始化处理器
        if file:
            file_handler = logging.FileHandler(file)
            file_handler.setLevel(level)
            file_handler.setFormatter(fmt)
            self.addHandler(file_handler)
        stream_handler = logging.StreamHandler()

        # 设置 handler 级别
        stream_handler.setLevel(level)
        stream_handler.setFormatter(fmt)
        self.addHandler(stream_handler)


logger = MyLog("Log", file="test_log.txt")

if __name__ == '__main__':
    logger = MyLog()
    logger.debug("hello")
