#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: 05_demo_excel
@time: 2021/11/27 16:45
"""
from openpyxl import load_workbook
from openpyxl.worksheet import worksheet

"""
1.打开表单
2.读取标题
3.读取所有的数据
4.指定单元格写入数据（使用静态方法）
"""


class ExcelHandler:
    """操作Excel"""

    def __init__(self, file):
        """
        初始化函数
        :param file: 路径
        """
        self.file = file

    def open_sheet(self, name) -> worksheet:
        """打开表单"""
        # 在函数或方法的后面加 -> 类型：表示此函数返回值是一个 这样的类型
        wb = load_workbook(self.file)
        excel_sheet = wb[name]
        return excel_sheet

    def header(self, sheet_name):
        """获取表单的表头"""
        sheet_name = self.open_sheet(sheet_name)
        headers = []
        for i in sheet_name[1]:
            headers.append(i.value)
        return headers

    def read(self, sheet_name):
        """读取所有的数据"""
        sheet_data = self.open_sheet(sheet_name)
        rows = list(sheet_data.rows)
        # 获取标题
        data = []
        for row in rows[1:]:
            row_data = []
            for cell in row:
                row_data.append(cell.value)
                # 列表转化成字典，需要和header函数去zip转化
                data_dict = dict(zip(self.header(sheet_name), row_data))
            data.append(data_dict)
        return data

    @staticmethod  # 不需要实例化，直接类名.方法名()来调用
    def write(file, sheet_name, row, column, data):
        """写入 Excel 数据"""
        wb = load_workbook(file)
        sheet_name = wb[sheet_name]
        # 修改单元格
        sheet_name.cell(row, column).value = data
        # 保存
        wb.save(file)
        # 关闭
        wb.close()


if __name__ == '__main__':
    excel = ExcelHandler(r'/case.xlsx')
    # read = excel.read('Sheet1')
    # print(read)
    excel.write(r'C:\Users\lipengfei\Desktop\api_test\case.xlsx', 'Sheet1', 4, 1, 'value')
