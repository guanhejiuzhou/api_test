#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: logger
@time: 2021/12/1 21:52
"""

import logging


# todo:日志封装

class LoggerHandler:

    def __init__(self, name='Log', level='DEBUG', file='log.txt',
                 fmt='%(filename)s:%(lineno)d-%(asctime)s:%(name)s:%(levelname)s-%(message)s'):
        logger = logging.getLogger(name)

        # 设置级别
        logger.setLevel(level)

        # 格式
        fmt = logging.Formatter(fmt)

        # 初始化处理器
        if file:
            file_handler = logging.FileHandler(file)
            file_handler.setLevel(level)
            file_handler.setFormatter(fmt)
            logger.addHandler(file_handler)
        stream_handler = logging.StreamHandler()

        # 设置 handler 级别
        stream_handler.setLevel(level)
        stream_handler.setFormatter(fmt)
        logger.addHandler(stream_handler)

        self.logger = logger

    def info(self, msg):
        return self.logger.info(msg)

    def debug(self, msg):
        return self.logger.debug(msg)

    def warning(self, msg):
        return self.logger.warning(msg)

    def error(self, msg):
        return self.logger.error(msg)

    def critical(self, msg):
        return self.logger.critical(msg)


if __name__ == '__main__':
    logger = LoggerHandler()
    logger.debug("hello world")




