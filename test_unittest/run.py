#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: run
@time: 2021/11/24 15:15
"""
import os
import unittest
from test_unittest.test_cases import test_assert
from test_unittest.test_cases import test_setup

# todo: 测试用例运行
from test_unittest.test_cases.test_assert import TestLogin
from test_unittest.test_cases.test_setup import TestDome

"""
收集测试用例：TestLoader，加载器，加载测试用例
放到测试集合（测试套件）TestSuite

1.初始化testloader
2.suite = testloader.discover(文件夹路径, '匹配模式，默认是以test开头')发现测试用例
3.如果有想运行的用例，就放到指定的文件夹中
"""
# 1.初始化testloader
testloader = unittest.TestLoader()

"""
几种加载测试用例方式：
1.用的最多，整个项目一起加载，使用：discover
2.只测试某一个具体的模块、类使用 loadTestsFromModule、loadTestsFromTestCase
"""

# 查找测试用例，加载
dir_path = os.path.dirname(os.path.abspath(__file__))
case_path = os.path.join(dir_path, 'test_cases')
# suite = testloader.discover(case_path)
# print(suite)

# todo:添加指定的测试模块
# suite1 = testloader.loadTestsFromModule(test_assert)
# suite2 = testloader.loadTestsFromModule(test_setup)

# todo:添加指定的测试类
suite3 = testloader.loadTestsFromTestCase(TestLogin)
suite4 = testloader.loadTestsFromTestCase(TestDome)

# 将多个测试套件合并添加到一个总的测试套件中  先初始化
suite_all = unittest.TestSuite()
suite_all.addTests(suite3)
suite_all.addTests(suite4)

report_path = os.path.join(dir_path, 'report')
# 如果report文件夹不存在，就创建一个
if not os.path.exists(report_path):
    os.mkdir(report_path)

file_path = os.path.join(report_path, 'test_result.txt')
with open(file_path, "w", encoding='utf-8') as f:
    # 初始化运行器，是以普通文本生成测试报告 TextTestRunner
    runner = unittest.TextTestRunner(f, verbosity=2)
    # 运行测试用例
    runner.run(suite_all)


