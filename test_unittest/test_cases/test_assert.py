#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: test_assert.py
@time: 2021/11/23 16:09
"""

# todo:断言

import unittest

"""
断言方式：
assertEqual,提示方式具体，会把预期结果和实际结果打印出来
assertTrue, 判断条件
assertGreater, a > b
assertIn, a in b
assertRegex, 正则表达式匹配
"""


class TestLogin(unittest.TestCase):

    def test_login_success(self):
        excepted = 1
        actual = 2
        # self.assertEqual(excepted, actual)
        self.assertTrue(excepted == actual)

    def test_login_pass(self):
        print("hello world")


if __name__ == "__main__":
    unittest.main()
