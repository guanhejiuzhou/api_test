#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: test_setup.py
@time: 2021/11/23 16:51
"""
import unittest

"""
setUp:前置条件，每次测试方法执行之前都会执行的方法
可以把测试数据等放在setUp当中

tearDown:后置条件，每次用例方法执行之后都会执行的方法

过程：先执行setUp()方法，然后执行test_...测试用例方法，最后执行tearDown()方法
"""


class TestDome(unittest.TestCase):

    def test_dome_success(self):
        excepted = 1
        actual = 2
        # self.assertEqual(excepted, actual)
        self.assertTrue(excepted == actual)

    def test_dome_pass(self):
        print("hello world")


if __name__ == "__main__":
    unittest.main()
