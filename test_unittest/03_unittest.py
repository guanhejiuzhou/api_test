#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: 03_unittest.py
@time: 2021/11/22 17:26
"""

# todo: 1.模块名、文件名以 test 开头
# todo: 2.类名以 Test 开头，如：TestLogin
# todo: 3.方法以 test 开头

import unittest


class TestLogin(unittest.TestCase):

    def test_login_1_success(self):
        # print("111")
        try:
            self.assertEqual(1, 3-2)
        except AssertionError as e:
            # 如果想断言失败，一定要抛出一个AssertionError
            print("断言失败", e)
            raise AssertionError

    def test_login_2_error(self):
        self.assertEqual(1, 2)


if __name__ == "__main__":
    unittest.main()

