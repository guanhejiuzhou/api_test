import requests

# 发送post请求
url = "http://localhost"
res = requests.post(url)

# 发送header请求头
headers = {"dalao": "y", "da": "x"}
res = requests.post(url, headers=headers)

# 1.传递参数 params形式
data = {"username": "u", "pwd": "123"}
res = requests.post(url, headers=headers, params=data)

# 2.传递参数：表单形式
form_data = {"username": "admin"}
res = requests.post(url, data=form_data, headers=headers, params=data)

# 3.传递参数：json
json_data = {"username": "admin1"}
res = requests.post(url, json=json_data, headers=headers)

# 获取响应文本，得到的数据类型 string
print(res.text)

# 获取二进制形式
print(res.content)

# 获取json，如果不是json的响应数据就会报错
# json得到的是 字典 数据类型
print(type(res.json()))



