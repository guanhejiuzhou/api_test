#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: 06_logging
@time: 2021/11/30 10:43
"""

import logging

"""
logging.info("这是一个普通信息")
logging.debug("这是一个 debug 信息")
logging.warning("这是一个警告信息")
logging.error("出错了")
logging.critical("奔溃了")
"""

# 初始化 logger 收集器
logger = logging.getLogger("testLog")

# 设置级别
logger.setLevel('DEBUG')

# 日志存放路径
handler = logging.FileHandler('log.txt')

# 添加handler
logger.addHandler(handler)

# handler设置格式
fmt = logging.Formatter('%(filename)s - %(lineno)d - %(asctime)s - %(name)s-%(levelname)s-%(message)s')
handler.setFormatter(fmt)

logger.info('hello')
logger.debug('world')
