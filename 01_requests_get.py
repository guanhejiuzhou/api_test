import requests

# 访问一个url接口地址，发送一个get请求
# url = 'http://api.github.com'
# res = requests.get(url)
# print(res.text)

# 如何传递参数、如何修改请求头，token
headers = {"token": '123', "username": "徐凤年"}
url = 'http://localhost'
res = requests.get(url, headers=headers)

# 如何传递参数：位置参数或者关键字参数params
data = {"username": "徐凤年", "pwd": "123"}
res = requests.get(url, params=data, headers=headers)
