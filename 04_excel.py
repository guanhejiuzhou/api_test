#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: 04_excel
@time: 2021/11/26 14:19
"""

import openpyxl

# 读取Excel文件夹,读取文件之前一定要先关闭文件
wb = openpyxl.load_workbook(r'D:\小东西\接口自动化\api_test\case.xlsx')
# print(wb)
# 获取所有表单
sheets = wb.worksheets
# print(sheets)
# 获取某一个表单--通过索引获取
# sheet = wb.worksheets[0]
# print(sheet)
# 获取某一个表单--通过表单名称获取（这种方式会给警告，此方法或许会在以后版本无法使用）
# sheet = wb.get_sheet_by_name("Sheet1")
# print(sheet)
# 获取某一个表单--正规用法
sheet = wb['Sheet1']
# print(sheet)

# 读取单个单元格中的值,需要知道该单元格的行号和列号
cell = sheet.cell(1, 2)
print(cell.value)

# 获取行和列
# print(sheet[1])
# print(sheet['A'])
# 获取多行
#print(sheet[1:3])
# 获取值
for column in sheet[1]:
    print(column.value)

# 获取所有的数据
total_data = list(sheet.rows)
print(total_data)
for row in total_data:
    for cell in row:
        print(cell.value)

# 写入一个单元格
cell = sheet.cell(4, 1)
cell.value = 1234
# 保存，需要文件名
wb.save(r'D:\小东西\接口自动化\api_test\case.xlsx')
# 关闭
wb.close()
