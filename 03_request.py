#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: 03_request.py.py
@time: 2021/11/23 11:01
"""

import requests


# todo:封装request


class HTTPHandler:
    def __init__(self):
        self.session = requests.Session()

    def visit(self, url, method, params=None, data=None, json=None, **kwargs):
        """
        访问一个接口，可以使用get请求、post请求、put/delete
        :param url:请求地址
        :param method:请求方法
        :param params:请求参数
        :param data:请求参数
        :param json:请求参数
        :return:
        """
        res = self.session.request(method, url, params=params, data=data, json=json, **kwargs)
        try:
            return res.json()
        except ValueError:
            print("not json")
