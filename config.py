#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: config
@time: 2021/12/14 16:10
"""

from configparser import ConfigParser

import yaml


class Config(ConfigParser):

    def __init__(self, file, encoding='utf-8'):
        """
        参数
        :param file: 配置文件的路径
        """
        super().__init__()
        self.read(file, encoding=encoding)


def read_yaml(file, encoding='utf-8'):
    with open(file, encoding=encoding) as f:
        return yaml.load(f.read(), Loader=yaml.FullLoader)


def write_yaml(file, data, encoding='utf-8'):
    """
    写入yaml,如果有中文，需要allow_unicode=True
    """
    with open(file, encoding=encoding, mode='w') as f:
        yaml.dump(data, stream=f, allow_unicode=True)


if __name__ == '__main__':
    # 读取
    # config = Config('config.ini')
    # a = config.get('teachers', 'name')
    # print(a)
    # print(read_yaml('config.yaml'))

    # 读取yaml数据
    data = read_yaml('config.yaml')
    # 重新写入一个新的yaml文件
    write_yaml('config1.yaml', data)
